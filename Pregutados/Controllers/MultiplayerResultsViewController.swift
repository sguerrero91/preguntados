//
//  MultiplayerResultsViewController.swift
//  Pregutados
//
//  Created by Sebastian Guerrero F on 6/19/20.
//  Copyright © 2020 SG. All rights reserved.
//

import UIKit
import FirebaseFirestore

class MultiplayerResultsViewController: UIViewController {

    @IBOutlet var playerNicknames: [UILabel]!
    @IBOutlet var playerScores: [UILabel]!
    
    let db = Firestore.firestore()
    var roomCode = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getResults()
    }
    
    private func getResults() {
        
        db.collection("room-scores").document(roomCode).getDocument { (snapshot, error) in
            guard let dataDictionary = snapshot?.data() else {
                print("error getting results")
                return
            }
            
            var index = 0
            
            for result in dataDictionary {
                self.playerNicknames[index].text = result.key
                self.playerScores[index].text = "\(result.value)"
                index += 1
            }
        }
    }
}
