//
//  LoginViewController.swift
//  Pregutados
//
//  Created by Sebastian Guerrero F on 6/8/20.
//  Copyright © 2020 SG. All rights reserved.
//

import UIKit
import FirebaseAuth

class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    private func presentErrorAlert(message: String) {
        
        let alert = UIAlertController(
            title: "Error",
            message: message,
            preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alert.addAction(okAction)
        
        present(alert, animated: true) {
            self.emailTextField.text = ""
            self.passwordTextField.text = ""
        }
    }
    
    @IBAction func signInButtonPressed(_ sender: Any) {
        
        let email = emailTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            // error == nil => TODO ESTA BIEN
            // error != nil => HAY UN ERROR
            
            if error != nil {
                self.presentErrorAlert(message: error?.localizedDescription ?? "")
                print(error)
            }
            
            if result != nil {
                
                print(result)
                self.performSegue(withIdentifier: "toMainMenu", sender: nil)
            }
        }
    }
}
