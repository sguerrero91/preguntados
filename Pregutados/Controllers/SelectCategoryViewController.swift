//
//  SelectCategoryViewController.swift
//  Pregutados
//
//  Created by Sebastian Guerrero F on 6/11/20.
//  Copyright © 2020 SG. All rights reserved.
//

import UIKit

class SelectCategoryViewController: UIViewController {

    @IBOutlet weak var categoryTextField: UITextField!
    @IBOutlet weak var practiceButton: UIButton!
    
    let categoryPicker = UIPickerView()
    var selectedCategory:Category?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        categoryTextField.inputView = categoryPicker
        
        categoryPicker.dataSource = self
        categoryPicker.delegate = self
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @IBAction func practiceButtonPressed(_ sender: Any) {
        Networking.getQuestions(for: selectedCategory?.id ?? 0) { questions in
            self.performSegue(withIdentifier: "toSoloGame", sender: questions)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toSoloGame" {
            let questions = sender as! [Question]
            let destination = segue.destination as! SoloGameViewController
            destination.questions = questions
            destination.category = selectedCategory?.name
        }
    }
    
    @IBAction func unwindSelectCategory(_ segue: UIStoryboardSegue) {
        
    }
}

extension SelectCategoryViewController: UIPickerViewDataSource, UIPickerViewDelegate {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Category.getCategoryList().count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Category.getCategoryList()[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedCategory = Category.getCategoryList()[row]
        categoryTextField.text = Category.getCategoryList()[row].name
        practiceButton.isEnabled = true
    }
}

