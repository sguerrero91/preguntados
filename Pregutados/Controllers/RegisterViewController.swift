//
//  RegisterViewController.swift
//  Pregutados
//
//  Created by Sebastian Guerrero F on 6/9/20.
//  Copyright © 2020 SG. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth

class RegisterViewController: UIViewController {

    //MARK:- Outlets
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var lastnameTextField: UITextField!
    @IBOutlet weak var nicknameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let db = Firestore.firestore()
//
//        db.collection("test").document("123456789").setData([
//            "nombre" : "Sebas",
//            "apellido" : "Guerrero",
//            "edad": 29
//        ])

    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @IBAction func registerButtonPressed(_ sender: Any) {
        
        let email = emailTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        
        registerInFirebase(email: email, password: password)
        
    }
    
    private func presentErrorAlert(message: String) {
        
        let alert = UIAlertController(
            title: "Error",
            message: message,
            preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alert.addAction(okAction)
        
        present(alert, animated: true) {
            self.emailTextField.text = ""
            self.passwordTextField.text = ""
        }
    }
    
    private func registerInFirebase(email: String, password: String) {
        
        Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
            
            if error != nil {
                self.presentErrorAlert(message: error?.localizedDescription ?? "")
            }
            
            // crea una variable userResult si es que result no es nil
            if let userResult = result {
                // guardar la data en firestore
                self.saveUserData(userId: userResult.user.uid)
            }
        }
    }
    
    private func saveUserData(userId: String) {
        
        let name = nameTextField.text ?? ""
        let lastname = lastnameTextField.text ?? ""
        let nickname = nicknameTextField.text ?? ""
        
        let db = Firestore.firestore()
        
//        db.collection("users").document(userId).setData([
//            "name" : name,
//            "lastname" : lastname,
//            "nickname" : nickname
//        ])
        
        db.collection("users").document(userId).setData([
            "name" : name,
            "lastname" : lastname,
            "nickname" : nickname
        ]) { (error) in
            if error != nil {
                // borrar el usuario
                // mostrar alerta
//                try? Auth.auth().signOut()
                
                Auth.auth().currentUser?.delete(completion: { (error) in
                    if error == nil {
                        self.dismiss(animated: true, completion: nil)
                    }
                })
            }
            
            self.performSegue(withIdentifier: "toMainMenu", sender: nil)
            
        }
    }
    
}
