//
//  CreateRoomViewController.swift
//  Pregutados
//
//  Created by Sebastian Guerrero F on 6/16/20.
//  Copyright © 2020 SG. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth

class CreateRoomViewController: UIViewController {

    var isNewRoom = true
    var code: String!
    let db = Firestore.firestore()
    let networking = Networking()
    var questions:[Question] = []
    var userNickname = ""

    @IBOutlet weak var roomCodeLabel: UILabel!
    @IBOutlet var playerLabels: [UILabel]!
    @IBOutlet weak var startButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        networking.delegate = self
        
        if isNewRoom {
            code = randomString(length: 6)
            roomCodeLabel.text = code
            registerRoom(code: code)
        } else {
            roomCodeLabel.text = code
            listenForNewPlayers()
            startButton.isHidden = true
        }
        
        listenForQuestions()
    }
    
    func randomString(length: Int) -> String {
        let letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
    
    func getUserNickname(userId: String, completionHandler: @escaping (String) -> ()) {
        db.collection("users").document(userId).getDocument { (snapshot, error) in
            guard let userData = snapshot?.data(),
                let nickname = userData["nickname"] as? String else {
                    print("error in user")
                    return
            }
            self.userNickname = nickname
            completionHandler(nickname)
        }
    }
    
    func registerRoom(code: String) {
        
        let user = Auth.auth().currentUser!
        
        getUserNickname(userId: user.uid) { (nickname) in
            self.db.collection("room-players").document(code).setData([
                "players" : [
                    nickname
                ]
            ]) { (error) in
                if error != nil {
                    print(error)
                }
                
                self.listenForNewPlayers()
            }
        }
    }
    
    func listenForQuestions() {
        db.collection("room-questions").document(code).addSnapshotListener { (snapshot, error) in
            guard let document = snapshot else {
                print("Questions error")
                return
            }
            
            guard let data = document.data() else {
                print("Error in data")
                return
            }
            
            guard let dataDictionary = data as? [String: [[String:Any]]] else {
                print("Error in Dictionary")
                return
            }
            
            self.mapQuestions(rawQuestions: dataDictionary["questions"] ?? [])
        }
    }
    
    func mapQuestions(rawQuestions: [[String:Any]]) {
        
        for rawQuestion in rawQuestions {
            
            let question = Question(
                category: rawQuestion["category"] as! String,
                type: rawQuestion["type"] as! String,
                difficulty: rawQuestion["difficulty"] as! String,
                question: rawQuestion["question"] as! String,
                correctAnswer: rawQuestion["correctAnswer"] as! String,
                incorrectAnswers: rawQuestion["incorrectAnswers"] as! [String]
            )
            
            questions += [question]
        }
        
        performSegue(withIdentifier: "toGame", sender: nil)
        
    }
    
    func listenForNewPlayers() {
        
        db.collection("room-players").document(code).addSnapshotListener { (snapshot, error) in
            guard let document = snapshot else {
                print("Document error")
                return
            }
            
            guard let data = document.data() else {
                print("Error in data")
                return
            }
            
            
            guard let dataDictionary = data as? [String: [String]] else {
                print("Error in Dictionary")
                return
            }
            
            guard let players = dataDictionary["players"] else {
                print("Error in players")
                return
            }
            
            if players.count < 4 {
                for i in (players.count)...3 {
                    self.playerLabels[i].text = "...waiting"
                }
            }
            
            for (index, value) in players.enumerated() {
                self.playerLabels[index].text = value
            }
        }
    }
    
    
    @IBAction func exitButtonPressed(_ sender: Any) {
        
        if isNewRoom {
            dismiss(animated: true, completion: nil)
        } else {
            
            let user = Auth.auth().currentUser!.uid
            
            db.collection("room-players").document(code).updateData([
                "players": FieldValue.arrayRemove([user])
            ]) { (error) in
                if error == nil {
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func copyCodeButtonPressed(_ sender: Any) {
        
    }
    
    @IBAction func startButtonPressed(_ sender: Any) {
        networking.getMultiplayerQuestions()
        
        db.collection("room-scores").document(code).setData([:])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toGame" {
            let destination = segue.destination as! SoloGameViewController
            destination.isMultiplayer = true
            destination.questions = questions
            destination.isRoomOwner = isNewRoom
            destination.roomCode = code
            destination.userNickname = userNickname
        }
    }
}

extension CreateRoomViewController: NetworkingDelegate {
    
    func fetchedQuestions(questions: [Question]) {
        
        let firestoreQuestions = questions.map {
            [
                "question" : $0.question,
                "correctAnswer" : $0.correctAnswer,
                "category" : $0.category,
                "incorrectAnswers" : $0.incorrectAnswers,
                "type" : $0.type,
                "difficulty" : $0.difficulty
            ]
        }
        
        db.collection("room-questions").document(code).setData([
            "questions" : firestoreQuestions
        ]) { (error) in
            if error != nil {
                print(error)
            }
        }
    }

}
