//
//  SoloGameResultViewController.swift
//  Pregutados
//
//  Created by Sebastian Guerrero F on 6/16/20.
//  Copyright © 2020 SG. All rights reserved.
//

import UIKit

class SoloGameResultViewController: UIViewController {

    var answers: Int?
    var score: Int?
    
    @IBOutlet weak var answersLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        answersLabel.text = "\(answers ?? 0)"
        scoreLabel.text = "\(score ?? 0)"
    }
}
