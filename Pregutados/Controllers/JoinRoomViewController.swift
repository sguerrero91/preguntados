//
//  JoinRoomViewController.swift
//  Pregutados
//
//  Created by Sebastian Guerrero F on 6/17/20.
//  Copyright © 2020 SG. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth

class JoinRoomViewController: UIViewController {
    
    @IBOutlet weak var codeTextField: UITextField!
    
    let db = Firestore.firestore()
    var userNickname = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    private func presentErrorAlert(message: String) {
        
        let alert = UIAlertController(
            title: "Error",
            message: message,
            preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alert.addAction(okAction)
        
        present(alert, animated: true) {
            self.codeTextField.text = ""
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toRoom" {
            let destination = segue.destination as! CreateRoomViewController
            destination.code = codeTextField.text ?? ""
            destination.isNewRoom = false
            destination.userNickname = userNickname
        }
    }
    
    func getUserNickname(userId: String, completionHandler: @escaping (String) -> ()) {
        db.collection("users").document(userId).getDocument { (snapshot, error) in
            guard let userData = snapshot?.data(),
                let nickname = userData["nickname"] as? String else {
                    print("error in user")
                    return
            }
            self.userNickname = nickname
            completionHandler(nickname)
        }
    }
    
    @IBAction func joinButtonPressed(_ sender: Any) {
        
        let code = codeTextField.text ?? ""
        let user = Auth.auth().currentUser!.uid
        
        getUserNickname(userId: user) { (nickname) in
            
            self.db.collection("room-players").document(code).updateData([
                "players": FieldValue.arrayUnion([nickname])
            ]) { (error) in
                
                if error == nil {
                    self.performSegue(withIdentifier: "toRoom", sender: nil)
                }
                
                if let dataError = error {
                    if dataError.localizedDescription.starts(with: "No document to update:") {
                        self.presentErrorAlert(message: "There is no room with code: \(code)")
                    }
                }
            }
        }
    }
}
