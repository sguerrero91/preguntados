//
//  SoloGameViewController.swift
//  Pregutados
//
//  Created by Sebastian Guerrero F on 6/12/20.
//  Copyright © 2020 SG. All rights reserved.
//

import UIKit
import FirebaseFirestore

class SoloGameViewController: UIViewController {

    var isMultiplayer = false
    var isRoomOwner = true
    var roomCode = ""
    var userNickname = ""
    
    var questions:[Question]!
    var category:String!
    var currentQuestionIndex = 0 {
        didSet {
            currentQuestionLabel.text = "\(currentQuestionIndex + 1) of 5"
        }
    }
    var currentQuestion:Question!
    var currentAnswers:[String]!
    var correctAnswers = 0
    var timer:Timer!
    var score = 0 // 50 * time left
    
    var timeLeft = 10 {
        didSet {
            timerLabel.text = "\(timeLeft)"
            if timeLeft >= 4 {
                timerLabel.textColor = .systemGreen
            } else {
                timerLabel.textColor = .systemRed
            }
        }
    }
    
    @IBOutlet weak var continueButton: UIButton!
    
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var currentQuestionLabel: UILabel!
    
    
    @IBOutlet var answerButtons: [UIButton]!
    
    let db = Firestore.firestore()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        categoryLabel.text = category
        
        if isMultiplayer {
            db.collection("room-game").document(roomCode).addSnapshotListener { (snapshot, error) in
                
                guard let newIndex = snapshot?.get("currentQuestionIndex") as? Int else {
                    return
                }
                
                self.currentQuestionIndex = newIndex
                self.loadQuestion()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadQuestion()
    }
    
    private func loadQuestion() {
        
        currentQuestion = questions[currentQuestionIndex]
        categoryLabel.text = currentQuestion.category
        
        continueButton.isHidden = true
        
        timeLeft = 10
        
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
            self.timeLeft -= 1
            
            if self.timeLeft == 0 {
                timer.invalidate()
                self.endQuestion()
            }
        }
        
        questionLabel.text = String(htmlEncodedString: currentQuestion.question)
        
        currentAnswers = currentQuestion.incorrectAnswers + [currentQuestion.correctAnswer]
        
        currentAnswers.shuffle()
        
        for i in 0...3 {
            answerButtons[i].setTitle(currentAnswers[i], for: .normal)
            answerButtons[i].setTitle("Última palabra?", for: .highlighted)
        }
        
        for button in answerButtons {
            button.isEnabled = true
            button.backgroundColor = .systemBlue
        }
    }
    
    private func saveScore() {
        db.collection("room-scores").document(roomCode).updateData([
            userNickname: score
        ]) { (error) in
            if error != nil {
                print(error)
            }
        }
    }
    
    @IBAction func answerButtonPressed(_ sender: Any) {
        
        let button = sender as! UIButton
        
        if currentAnswers[button.tag] == currentQuestion.correctAnswer {
            correctAnswers += 1
            score += (timeLeft * 50)
        }
        
        endQuestion()
    }
    
    @IBAction func continueButtonPressed(_ sender: Any) {
        
        if isMultiplayer && currentQuestionIndex == 4 {
            performSegue(withIdentifier: "toResults", sender: nil)
            return
        }
        
        if currentQuestionIndex == 4 {
            performSegue(withIdentifier: "toEndGame", sender: nil)
            return
        }
        
        currentQuestionIndex += 1

        if isMultiplayer {
            db.collection("room-game").document(roomCode).setData(["currentQuestionIndex": currentQuestionIndex])
            return
        }
        
        loadQuestion()
    }
    
    private func endQuestion() {
        continueButton.isHidden = !isRoomOwner
        
        timer.invalidate()
        
        for button in answerButtons {
            button.isEnabled = false
            
            if button.title(for: .normal) == currentQuestion.correctAnswer {
                button.backgroundColor = .systemGreen
            } else {
                button.backgroundColor = .systemRed
            }
        }
        
        if isMultiplayer {
            saveScore()
            
            if currentQuestionIndex == 4 {
                continueButton.isHidden = false
            }
            
        }
    }
    
    @IBAction func exitButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toEndGame" {
            let destination = segue.destination as! SoloGameResultViewController
            destination.answers = correctAnswers
            destination.score = score
        }
        
        if segue.identifier == "toResults" {
            let destination = segue.destination as! MultiplayerResultsViewController
            destination.roomCode = roomCode
        }
    }
}
