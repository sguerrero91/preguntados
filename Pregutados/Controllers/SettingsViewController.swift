//
//  SettingsViewController.swift
//  Pregutados
//
//  Created by Sebastian Guerrero F on 6/24/20.
//  Copyright © 2020 SG. All rights reserved.
//

import UIKit
import FirebaseAuth

class SettingsViewController: UIViewController {

    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func logoutButtonPressed(_ sender: Any) {
        try? Auth.auth().signOut()
    }
}
