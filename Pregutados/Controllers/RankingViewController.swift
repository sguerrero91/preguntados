//
//  RankingViewController.swift
//  Pregutados
//
//  Created by Sebastian Guerrero F on 6/19/20.
//  Copyright © 2020 SG. All rights reserved.
//

import UIKit
import FirebaseFirestore

class RankingViewController: UIViewController {
    
    var playerNicknames:[String] = []
    var playerScores:[Int] = []

    let db = Firestore.firestore()
    
    @IBOutlet weak var resultsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        db.collection("general").document("ranking").addSnapshotListener { (snapshot, error) in
            guard let playerScores = snapshot?.data() as? [String: Int] else {
                print("error on player score")
                return
            }
            
            self.playerScores = []
            self.playerNicknames = []
            
            for score in playerScores {
                self.playerScores += [score.value]
                self.playerNicknames += [score.key]
            }
            
            self.resultsTableView.reloadData()
        }
    }
}

extension RankingViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playerNicknames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "rankingCell") as! RankingTableViewCell
        
        cell.nicknameLabel.text = playerNicknames[indexPath.row]
        cell.scoreLabel.text = "\(playerScores[indexPath.row])"
        
        return cell
    }
}

extension RankingViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Section \(section + 1)"
    }
    
}
