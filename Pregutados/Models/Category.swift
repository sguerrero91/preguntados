//
//  Category.swift
//  Pregutados
//
//  Created by Sebastian Guerrero F on 6/11/20.
//  Copyright © 2020 SG. All rights reserved.
//

import Foundation

struct Category {
    var id: Int
    var name: String
}


extension Category {
    
    static func getCategoryList() -> [Category] {
        return [
            Category(id: 9, name: "General Knowledge"),
            Category(id: 18, name: "Science: Computers"),
            Category(id: 21, name: "Sports"),
            Category(id: 27, name: "Animals"),
            Category(id: 15, name: "Entertainment: Video Games"),
            Category(id: 12, name: "Entertainment: Music")
        ]
    }
}
