//
//  QuestionsResponse.swift
//  Pregutados
//
//  Created by Sebastian Guerrero F on 6/12/20.
//  Copyright © 2020 SG. All rights reserved.
//

import Foundation

// MARK: - QuestionsResult
struct QuestionsResult: Codable {
    let responseCode: Int
    let results: [Question]

    enum CodingKeys: String, CodingKey {
        case responseCode = "response_code"
        case results = "results"
    }
}

// MARK: - Result
struct Question: Codable {
    let category: String
    let type: String
    let difficulty: String
    let question: String
    let correctAnswer: String
    let incorrectAnswers: [String]

    enum CodingKeys: String, CodingKey {
        case category = "category"
        case type = "type"
        case difficulty = "difficulty"
        case question = "question"
        case correctAnswer = "correct_answer"
        case incorrectAnswers = "incorrect_answers"
    }
}

