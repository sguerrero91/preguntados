//
//  Networking.swift
//  Pregutados
//
//  Created by Sebastian Guerrero F on 6/12/20.
//  Copyright © 2020 SG. All rights reserved.
//

import Foundation
import Alamofire

protocol NetworkingDelegate {
    func fetchedQuestions(questions: [Question])
}


class Networking {
    
    var delegate: NetworkingDelegate!
    
    static func getQuestions(for categoryId: Int, completionHandler: @escaping ([Question])->() ) {
        
        AF.request("https://opentdb.com/api.php?amount=5&type=multiple&category=\(categoryId)").response { result in
            //result -> data
            
            let decoder = JSONDecoder()
            
            guard let data = result.data, let questions = try? decoder.decode(QuestionsResult.self, from: data) else {
                print("ERROR")
                return
            }
            
            completionHandler(questions.results)
        }
    }
    
    
    func getMultiplayerQuestions() {
        
        AF.request("https://opentdb.com/api.php?amount=5&type=multiple").response { result in
            
            let decoder = JSONDecoder()
            
            guard let data = result.data, let questions = try? decoder.decode(QuestionsResult.self, from: data) else {
                print("ERROR")
                return
            }
            
            self.delegate.fetchedQuestions(questions: questions.results)
        }
    }
}
